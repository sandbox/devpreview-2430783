<?php

/**
 * @file
 * Contains \Drupal\block_view\Plugin\Block\BlockViewBlock.
 */

namespace Drupal\block_view\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Entity\View as ViewEntity;

/**
 * Defines a generic view block type.
 *
 * @Block(
 *  id = "block_view",
 *  admin_label = @Translation("View block"),
 *  category = @Translation("Views")
 * )
 */
class BlockViewBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Plugin Block Manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface.
   */
  protected $blockManager;

  /**
   * The current route match.
   * 
   * @var \Drupal\Core\Routing\RouteMatchInterface 
   */
  protected $routeMatch;

  /**
   * The Drupal account to use for checking for access to block.
   *
   * @var \Drupal\Core\Session\AccountInterface.
   */
  protected $account;

  /**
   * Constructs a new BlockViewBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Block\BlockManagerInterface
   *   The Plugin Block Manager.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which view access should be checked.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BlockManagerInterface $block_manager, RouteMatchInterface $route_match, AccountInterface $account) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->blockManager = $block_manager;
    $this->routeMatch = $route_match;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('plugin.manager.block'), $container->get('current_route_match'), $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['view_id'] = null;
    $config['view_display_id'] = null;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $options = array();
    foreach (ViewEntity::loadMultiple() as $name => $view) {
      /* @var $view \Drupal\views\Entity\View */
      $options[$view->label()] = array();
      foreach ($view->get('display') as $display_id => $display) {
        if ($display_id == 'default') {
          continue;
        }
        $options[$view->label()][$name . ':' . $display_id] = $view->label() . ' - ' . $display['display_title'];
      }
    }
    $form['view'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('View'),
      '#description' => t('Output the block in this view mode.'),
      '#default_value' => $this->configuration['view_id'] . ':' . $this->configuration['view_display_id']
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $view = explode(':', $form_state->getValue('view'));
    if (count($view) > 1) {
      $this->configuration['view_id'] = $view[0];
      $this->configuration['view_display_id'] = $view[1];
      $this->blockManager->clearCachedDefinitions();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $view = ViewEntity::load($this->configuration['view_id']);
    $display_id = $this->configuration['view_display_id'];
    if (!$view) {
      return array('#markup' => t('View not found.'), '#access' => $this->account->hasPermission('administer blocks'));
    }
    if (!$view->getDisplay($display_id)) {
      return array('#markup' => t('View display not found.'), '#access' => $this->account->hasPermission('administer blocks'));
    }
    $args = array();
    foreach ($this->routeMatch->getParameters()->all() as $name => $attribute) {
      if (substr($name, 0, 4) == 'arg_') {
        $args[substr($name, 4)] = $attribute;
      }
    }
    $view_executable = $view->getExecutable();
    $view_executable->setDisplay($display_id);
    if (count($args) < (count($view_executable->display_handler->getArgumentsTokens()) / 2)) {
      return array('#markup' => t('View display require arguments.'), '#access' => $this->account->hasPermission('administer blocks'));
    }
    $view_executable->args = $args;
    return $view_executable->display_handler->execute();
    //$build['#view']->access($display_id, $this->account);
  }

}
